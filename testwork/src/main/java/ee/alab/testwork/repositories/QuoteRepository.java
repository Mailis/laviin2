package ee.alab.testwork.repositories;

import ee.alab.testwork.models.Quote;
import ee.alab.testwork.models.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuoteRepository  extends JpaRepository<Quote, Long> {
    Quote findOneByValue(Value quoteValue);

    @Query(value="SELECT * FROM quote WHERE value_id = ?1", nativeQuery = true)
    List<Quote> existsByValueId(long valueId);
}
