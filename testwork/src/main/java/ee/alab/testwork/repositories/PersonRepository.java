package ee.alab.testwork.repositories;

import ee.alab.testwork.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface PersonRepository  extends JpaRepository<Person, Long> {

    Person findOneByEmail(String email);

    @Query(value="SELECT * FROM person p WHERE p.email = ?1", nativeQuery = true)
    Person existsByEmail(String email);

    @Transactional
    @Modifying
    @Query(value="INSERT INTO person_role  (person_id, role_id) VALUES (?1, ?2)", nativeQuery = true)
    void addRoleToPerson(Long personId, Long roleId);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM person_role WHERE person_id = ?1 AND role_id = ?2 ", nativeQuery = true)
    void removeRoleFromPerson(Long personId, Long roleId);
}
