package ee.alab.testwork.repositories;

import ee.alab.testwork.models.Value;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ValueRepository  extends JpaRepository<Value, Long> {
}
