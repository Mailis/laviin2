package ee.alab.testwork.repositories;

import ee.alab.testwork.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface RoleRepository extends JpaRepository <Role, Long> {
    List<Role> findAll();

    Role findOneByName(String roleName);
}
