package ee.alab.testwork;

import ee.alab.testwork.models.Person;
import ee.alab.testwork.models.UserRoleType;
import ee.alab.testwork.security.JWTAuthorizationFilter;
import ee.alab.testwork.services.PersonService;
import ee.alab.testwork.services.RegisterService;
import ee.alab.testwork.services.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

@SpringBootApplication
public class TestworkApplication {
	final Logger logger = LoggerFactory.getLogger(TestworkApplication.class);

	@Autowired
	private PersonService personService;
	@Autowired
	private RegisterService registerService;
	@Autowired
	private RoleService roleService;

	public static void main(String[] args) {
		SpringApplication.run(TestworkApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void runAfterStartup() {
		List<Person> allPersons = this.personService.findAll();
		logger.info("Number of persons: {}", allPersons.size());

		var newPerson = new Person();
		newPerson.setFirstName("John");
		newPerson.setLastName("Doe");
		newPerson.setEmail("doe_admin3@gmail.com");
		newPerson.setPassword("DoePass");
		logger.info("Saving new person...");
		Person regPerson = personService.createPerson(newPerson);
		personService.addRole(regPerson, UserRoleType.GENERAL.name());
		personService.addRole(regPerson, UserRoleType.ADMIN.name());


		allPersons = this.personService.findAll();
		logger.info("Number of persons: {}", allPersons.size());
	}

	@EnableWebSecurity
	@Configuration
	static
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			String general = UserRoleType.GENERAL.name();
			String admin = UserRoleType.ADMIN.name();
			http.csrf().disable()
					.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
					.authorizeRequests()
					.mvcMatchers(HttpMethod.POST, "/login", "/register", "/roles*").permitAll()
					.mvcMatchers(HttpMethod.GET, "/roles/*").permitAll()
					.mvcMatchers(HttpMethod.GET, "/roles/{id}").permitAll()
					.mvcMatchers(HttpMethod.GET, "/roles/name/{role}").permitAll()

					.mvcMatchers(HttpMethod.GET,"/persosns/*").hasAnyRole(general, admin)
					.mvcMatchers(HttpMethod.GET,"/persosns/{id}").hasAnyRole(general, admin)
					.mvcMatchers(HttpMethod.POST,"/persosns").hasRole(admin)
					.mvcMatchers(HttpMethod.POST,"/persosns/{personId}/{roleId}").hasRole(admin)
					.mvcMatchers(HttpMethod.DELETE,"/persosns/{personId}/{roleId}").hasRole(admin)

					.mvcMatchers("/exchangerate/*",  "/quotes*").hasAnyRole(general, admin)
					.anyRequest().authenticated();
		}
	}
}
