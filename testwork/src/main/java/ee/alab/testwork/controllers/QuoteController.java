package ee.alab.testwork.controllers;

import ee.alab.testwork.models.Quote;
import ee.alab.testwork.models.Value;
import ee.alab.testwork.models_request.QuoteRequest;
import ee.alab.testwork.services.QuoteService;
import ee.alab.testwork.services.ValueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.util.Collections;
import java.util.List;

@RequestMapping(value = "/quotes", produces = { "application/json", "application/hal+json" })
@RestController
public class QuoteController {
    Logger logger = LoggerFactory.getLogger(QuoteController.class);

    @Autowired
    QuoteService quoteService;

    @Autowired
    ValueService valueService;

    @GetMapping("/list")
    public ResponseEntity<List<Quote>> index() {
        List<Quote> qts  = quoteService.findAll();
        return new ResponseEntity<>(qts, HttpStatus.OK);
    }

    @GetMapping("/random")
    public ResponseEntity<QuoteRequest> quote(RestTemplate restTemplate)  throws NullPointerException {
        //noinspection MismatchedQueryAndUpdateOfCollection
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        QuoteRequest quote = restTemplate.getForObject(
                "https://quoters.apps.pcfone.io/api/random", QuoteRequest.class);

        Quote newQuote = new Quote();

        Value value = quote.getValue();
        if(value == null){
            return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
        }
        long valueId = value.getId();

        Value existingValue = valueService.findById(valueId);
        if(existingValue == null) {
            value = valueService.saveValue(value);
        }
        else{
            value = existingValue;
        }

        if(!quoteService.quoteWithValueExists(value)){
            newQuote.setValue(value);
            newQuote.setType(quote.getType());
            quoteService.saveQuote(newQuote);
        }

        return new ResponseEntity<>(quote, HttpStatus.OK);
    }
}
