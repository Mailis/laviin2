package ee.alab.testwork.controllers;

import ee.alab.testwork.services.ExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;


@RequestMapping(value = "/exchangerate", produces = { "application/json", "application/hal+json" })
@RestController
public class ExchangeController {
    Logger logger = LoggerFactory.getLogger(ExchangeController.class);

    @Autowired
    ExchangeService exchangeService;


    @GetMapping("/{assetIdBase}/{assetIdQuote}")
    public ResponseEntity<Mono<String>> getExchangeRate(@PathVariable String assetIdBase, @PathVariable String assetIdQuote){
        String url = "https://rest.coinapi.io/v1/exchangerate/"+ assetIdBase + "/" + assetIdQuote;
        Mono<String> resp = exchangeService.sendGetRequest(url);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @GetMapping("/history/{assetIdBase}/{assetIdQuote}/{yearStart}")
    public ResponseEntity<Mono<String>> getExchangeRateHistory(@PathVariable String assetIdBase, @PathVariable String assetIdQuote, @PathVariable String yearStart){
        String url = "https://rest.coinapi.io/v1/ohlcv/"+ assetIdBase + "/" + assetIdQuote + "/history?period_id=1MTH&time_start=" + yearStart + "-01-01T00:00:00";
        Mono<String> resp = exchangeService.sendGetRequest(url);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }
}
