package ee.alab.testwork.controllers;

import ee.alab.testwork.models.Role;
import ee.alab.testwork.models_request.RoleRequest;
import ee.alab.testwork.services.RoleService;
import ee.alab.testwork.utils.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/roles")
@RestController
public class RoleController {
    final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    RoleService roleService;

    @GetMapping
    public ResponseEntity<List<Role>> index(){
        List<Role> allroles = roleService.findAllRoles();
        logger.info("ALL ROLES + {}", allroles);
        return new ResponseEntity<>(roleService.findAllRoles(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Role> getRoleById(@PathVariable Long id){
        var r = roleService.findRoleById(id);
        logger.info("getRoleById role id: {}",  id );
        return new ResponseEntity<>(r, r != null? HttpStatus.OK : HttpStatus.GONE);
    }

    @GetMapping("/name/{role}")
    public ResponseEntity<Role> getRoleByRoleName(@PathVariable String role){
        logger.info( "getRoleByRoleName role: {}",  role );
        var uRole = roleService.findRoleByName(role);
        return new ResponseEntity<>(uRole, uRole != null? HttpStatus.OK : HttpStatus.GONE);
    }

    @PostMapping
    public ResponseEntity<Role> createRole(@RequestBody RoleRequest role ){
        logger.info( "createRole role: {}",  role);
        Role r = ObjectMapper.mapRoleRequestToRole(role);
        return new ResponseEntity<>(roleService.createRole(r), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteRole(@PathVariable Long id){
        logger.info ("deleteRole roleId: {}",  id);
        boolean isSuccessfulDelete = roleService.deleteRoleById(id);
        return new ResponseEntity<>(isSuccessfulDelete, isSuccessfulDelete? HttpStatus.OK : HttpStatus.CONFLICT);
    }
}
