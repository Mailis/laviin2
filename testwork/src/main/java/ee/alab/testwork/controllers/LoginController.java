package ee.alab.testwork.controllers;

import ee.alab.testwork.models_response.PersonResponse;
import ee.alab.testwork.security.JwtOperations;
import ee.alab.testwork.models.Person;
import ee.alab.testwork.services.PersonService;
import ee.alab.testwork.utils.PBKDF2Hasher;
import ee.alab.testwork.models_request.LoginData;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/login", produces = { "application/json", "application/hal+json" })
@RestController
public class LoginController {
    final Logger logger = LoggerFactory.getLogger(LoginController.class);
    final PBKDF2Hasher hasher = new PBKDF2Hasher();
    final ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private PersonService personService;

    @PostMapping
    public ResponseEntity<PersonResponse> loginUser(@RequestBody LoginData logindata) {
        logger.info("Logging in person request:  {}  {} ", logindata.getEmail(), logindata.getPassword());

        if (logindata.getPassword() == null || logindata.getEmail() == null) {
            logger.error("Logging in person BAD request ");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            Person p = personService.findPersonByEmail(logindata.getEmail());
            if(p != null) {
                String hashedPassWord = p.getPassword();
                boolean isValidPassword = hasher.checkPassword(logindata.getPassword(), hashedPassWord);
                if (isValidPassword) {
                    PersonResponse personResponse = modelMapper.map(p, PersonResponse.class);
                    String token = JwtOperations.generateToken(p);
                    personResponse.setToken(token);
                    logger.info("Logging in person TOKEN: {}",  token);
                    logger.info("Logging in person SUCCESS");
                    return new ResponseEntity<>(personResponse, HttpStatus.OK);
                }
                else{
                    logger.info("Logging in person is NULL");
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            }
            else{
                logger.info("Logging in person NOT FOUND");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
