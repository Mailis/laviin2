package ee.alab.testwork.controllers;

import ee.alab.testwork.models_request.PersonRequest;
import ee.alab.testwork.models_response.PersonResponse;
import ee.alab.testwork.services.RegisterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/register", produces = { "application/json", "application/hal+json" })
@RestController
public class RegisterController {
    final Logger logger = LoggerFactory.getLogger(RegisterController.class);
    @Autowired
    private RegisterService registerService;

    @PostMapping
    public ResponseEntity<PersonResponse> registerUser(@RequestBody PersonRequest person) {
        logger.info("Create person request: {} {} {} {}",
                                    person.getFirstName(),
                                    person.getLastName(),
                                    person.getEmail(),
                                    person.getPassword()
                   ) ;

        if(person.getFirstName() == null || person.getLastName() == null ||
                person.getEmail() == null || person.getPassword() == null){
            logger.error("Registering person BAD request ") ;
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            PersonResponse registeredPerson = registerService.registerNewUser(person);
            if(registeredPerson == null){
                return new ResponseEntity<>(HttpStatus.GONE);
            }
            else{
                return new ResponseEntity<>(registeredPerson, HttpStatus.OK);
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
