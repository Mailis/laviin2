package ee.alab.testwork.controllers;

import ee.alab.testwork.models.Person;
import ee.alab.testwork.models_request.PersonRequest;
import ee.alab.testwork.models_response.PersonResponse;
import ee.alab.testwork.services.PersonService;
import ee.alab.testwork.utils.IPersonResponseBuilder;
import ee.alab.testwork.utils.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/persons", produces = { "application/json", "application/hal+json" })
@RestController
public class PersonController extends IPersonResponseBuilder {
    final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private PersonService personService;

    @GetMapping
    public ResponseEntity<List<PersonResponse>> index() {
        List<PersonResponse> allPersons = ObjectMapper.mapPersonListToPersonResponseList(this.personService.findAll());
        logger.info("All persons: {}", allPersons.size());
        return new ResponseEntity<>(allPersons, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonResponse> getPersonById(@PathVariable Long id) {
        PersonResponse p = ObjectMapper.mapPersonToResponse(this.personService.findOneById(id));
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    @PostMapping("/{personId}/{roleId}")
    public ResponseEntity<PersonResponse> addRoleToPerson(@PathVariable Long personId,@PathVariable Long roleId) {
        PersonResponse p = ObjectMapper.mapPersonToResponse(this.personService.addRole(personId, roleId));
        logger.info("Create person request: firstname: {}; lastName: {}; roles: {}",
                p.getFirstName(), p.getLastName(), p.getRoles());

        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PersonResponse> createPerson(@RequestBody PersonRequest personRequest) {
        logger.info("Create person request: firstname: {} ; lastName: {}", personRequest.getFirstName(), personRequest.getLastName());

        if(personRequest.getFirstName() == null || personRequest.getLastName() == null){
            logger.error("Create person BAD request ") ;
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            PersonResponse personResponse = buildPersonResponse(personService, personRequest);
            logger.info("Create person SUCCESS") ;
            return new ResponseEntity<>(personResponse, personResponse != null? HttpStatus.OK : HttpStatus.GONE);
        }
        catch (Exception e){
            logger.error("Create person ERROR: {}", e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{personId}/{roleId}")
    public void removeRoleFromPerson(@PathVariable Long personId, @PathVariable Long roleId){
        personService.removeRoleFromPerson(personId,  roleId);
    }

    @DeleteMapping("/{personId}")
    public void deletePerson(@PathVariable Long personId){
        personService.deletePerson(personId);
    }

    @Override
    protected void addToken(PersonResponse personResponse, Person newPerson) {
    }
}
