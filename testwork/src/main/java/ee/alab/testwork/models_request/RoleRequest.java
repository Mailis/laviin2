package ee.alab.testwork.models_request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleRequest {
    String name;

    public RoleRequest(){
    }
    public RoleRequest(String name){
        this.name = name;
    }
}
