package ee.alab.testwork.models_request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.alab.testwork.models.Value;
import lombok.Getter;
import lombok.Setter;


@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class QuoteRequest {

    private String type;

    private Value value;

    @Override
    public String toString() {
        return "Quote{" +
                "type='" + type + '\'' +
                ", value=" + value +
                '}';
    }
}