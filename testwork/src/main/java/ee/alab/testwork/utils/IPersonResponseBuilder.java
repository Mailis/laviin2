package ee.alab.testwork.utils;

import ee.alab.testwork.models.Person;
import ee.alab.testwork.models.UserRoleType;
import ee.alab.testwork.models_request.PersonRequest;
import ee.alab.testwork.models_response.PersonResponse;
import ee.alab.testwork.services.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class IPersonResponseBuilder {
    private static final Logger logger = LoggerFactory.getLogger(IPersonResponseBuilder.class);

    public PersonResponse buildPersonResponse(PersonService personService, PersonRequest personRequest){
        if(personRequest == null){
            return null;
        }
        //save new person
        Person newPerson = personService.createPerson(ObjectMapper.mapPersonRequestToPerson(personRequest));

        //build response and add default role
        if(newPerson != null) {
            PersonResponse personResponse = ObjectMapper.mapPersonToResponse(newPerson);
            addToken(personResponse, newPerson);

            personResponse.setId(newPerson.getId());
            personResponse.setFirstName(newPerson.getFirstName());
            personResponse.setLastName(newPerson.getLastName());
            personResponse.setEmail(newPerson.getEmail());

            //add default role to the new person
            Person personWithDefaultRole = personService.addRole(newPerson, UserRoleType.GENERAL.name());
            personResponse.setRoles(personWithDefaultRole.getRoles());

            logger.info("Registering person SUCCESS");
            return personResponse;
        }
        else{
            return null;
        }
    }

    protected abstract  void addToken(PersonResponse personResponse, Person newPerson);
}
