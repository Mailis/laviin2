package ee.alab.testwork.utils;

import ee.alab.testwork.models.Person;
import ee.alab.testwork.models.Role;
import ee.alab.testwork.models_request.PersonRequest;
import ee.alab.testwork.models_request.RoleRequest;
import ee.alab.testwork.models_response.PersonResponse;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

public class ObjectMapper {

    static final ModelMapper modelMapper = new ModelMapper();

    public static PersonResponse mapPersonToResponse(Person p){
        return modelMapper.map(p, PersonResponse.class);
    }
    public static Person mapPersonResponseToPerson(PersonResponse p){
        return modelMapper.map(p, Person.class);
    }

    public static Person mapPersonRequestToPerson(PersonRequest p){
        return modelMapper.map(p, Person.class);
    }

    public static List<PersonResponse> mapPersonListToPersonResponseList(List<Person> pList){
        List<PersonResponse> respPersons = new ArrayList<>();
        pList.forEach(p ->
                    respPersons.add(mapPersonToResponse(p))
                );
        return respPersons;
    }

    public static Role mapRoleRequestToRole(RoleRequest roleRequest){
        return modelMapper.map(roleRequest, Role.class);
    }
}
