package ee.alab.testwork.utils;

import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import reactor.core.publisher.Mono;
import org.slf4j.Logger;

public class LoggerHelper {
    private LoggerHelper(){}

    public static ExchangeFilterFunction logRequest(Logger logger) {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            if (logger.isDebugEnabled()) {
                var sb = new StringBuilder("Request: \n");
                //append clientRequest method and url
                clientRequest
                        .headers()
                        .forEach((name, value) -> sb.append(name).append(": ").append(value));
                logger.debug(sb.toString());
            }
            return Mono.just(clientRequest);
        });
    }
}
