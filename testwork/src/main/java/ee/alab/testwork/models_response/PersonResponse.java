package ee.alab.testwork.models_response;

import com.fasterxml.jackson.annotation.JsonProperty;
import ee.alab.testwork.models.Role;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Getter
@Setter
public class PersonResponse{
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private Set<Role> roles;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String token;
}