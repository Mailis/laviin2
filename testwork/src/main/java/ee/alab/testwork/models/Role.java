package ee.alab.testwork.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@Entity
public class Role {
    @Id
    @GeneratedValue
    private long id;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    List<Person> person = new ArrayList<>();

    @Column(name = "role", nullable = false)
    String name;

    public Role(){
    }
    public Role(String name){
        this.name = name;
    }
}
