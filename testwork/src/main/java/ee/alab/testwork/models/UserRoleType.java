package ee.alab.testwork.models;

public enum UserRoleType {
    GENERAL,
    ADMIN
}
