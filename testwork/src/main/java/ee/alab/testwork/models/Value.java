package ee.alab.testwork.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@Entity
public class Value {
    @Id
    private Long id;

    private String quote;

    @JsonIgnore
    @OneToOne(mappedBy = "value")
    private Quote qoute;

}
