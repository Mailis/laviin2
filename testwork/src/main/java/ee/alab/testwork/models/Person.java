package ee.alab.testwork.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import ee.alab.testwork.utils.PBKDF2Hasher;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "person")
public class Person{
    private static final PBKDF2Hasher hasher = new PBKDF2Hasher();
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    public void setPassword(String password){
        this.password = hasher.hash(password.toCharArray());
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "person_role",
            joinColumns = {
                    @JoinColumn(name = "person_id", referencedColumnName = "id",
                            nullable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "role_id", referencedColumnName = "id",
                            nullable = false)})
    private Set<Role> roles = new HashSet<>();

}
