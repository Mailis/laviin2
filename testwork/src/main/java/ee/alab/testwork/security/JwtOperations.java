package ee.alab.testwork.security;

import ee.alab.testwork.models.Person;
import ee.alab.testwork.models.Role;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;
import java.util.stream.Collectors;

public class JwtOperations{
    private static final Logger logger = LoggerFactory.getLogger(JwtOperations.class);
    JwtOperations(){}

    public static String getJWTToken(Person person, Map<String, Object> claims) {

        String token = Jwts
                .builder()
                .setId("testJWT")
                .setAudience("All")
                .setSubject(person.getLastName())
                .setClaims(claims)
                .setIssuedAt(JwtConstants.NOW)
                .setIssuer(JwtConstants.ISSUER)
                .setNotBefore(JwtConstants.NOW)
                .setExpiration(JwtConstants.EXPIRATION)
                .signWith(SignatureAlgorithm.HS512,
                        JwtConstants.SECRET_KEY.getBytes()).compact();

        return JwtConstants.PREFIX + token;
    }


    public static String generateToken(Person p){
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", p.getRoles());
        claims.put("id", p.getId());
        claims.put("firstname", p.getFirstName());
        claims.put("lastname", p.getLastName());
        claims.put("email", p.getEmail());
        claims.put("authorities", getAuthorities(p.getRoles()));
        return getJWTToken(p, claims);
    }

    private static List<String> getAuthorities( Set<Role> roles) {
        List<GrantedAuthority> grantedAuthorities
                = new ArrayList<>();
        roles.stream()
                .map(r -> new SimpleGrantedAuthority(r.getName()))
                .forEach(grantedAuthorities::add);
        logger.info("getAuthorities grantedAuthorities: {}", grantedAuthorities);
        List<String> authorities = grantedAuthorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        logger.info("getAuthorities authorities: {}", authorities);
        return authorities;
    }

}
