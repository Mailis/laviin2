package ee.alab.testwork.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

public class JWTAuthorizationFilter extends OncePerRequestFilter{

    final Logger loger = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain){
        var errMessage = "doFilterInternal Claims get request ERROR: {}";
        loger.info("doFilterInternal param request: {}", request);
        try {
            if (checkJWTToken(request)) {
                var claims = validateToken(request);
                loger.info("doFilterInternal Claims get SUBJECT: {}", claims.getSubject());
                loger.info("doFilterInternal Claims get authorities: {}", claims.get(JwtConstants.AUTHORITIES));
                if (claims.get(JwtConstants.AUTHORITIES) != null) {
                    loger.info("doFilterInternal Claims get authorities != null");
                    setUpSpringAuthentication(claims);
                } else {
                    loger.info("doFilterInternal Claims get authorities == null");
                    SecurityContextHolder.clearContext();
                }
            } else {
                SecurityContextHolder.clearContext();
            }
            chain.doFilter(request, response);
        }
        catch (ServletException | IOException | ExpiredJwtException | UnsupportedJwtException | MalformedJwtException e) {
            loger.info(errMessage,  e.getMessage());
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
        catch (Exception e) {
            loger.info(errMessage,  e.getMessage());
        }
    }

    private Claims validateToken(HttpServletRequest request) {
        String jwtToken = request.getHeader(JwtConstants.HEADER).replace(JwtConstants.PREFIX, "");
        return Jwts.parser().setSigningKey(JwtConstants.SECRET_KEY.getBytes()).parseClaimsJws(jwtToken).getBody();
    }

    /**
     * Authentication method in Spring flow
     *
     */
    private void setUpSpringAuthentication(Claims claims) {
        @SuppressWarnings("unchecked")
        List<String> authorities = (List<String>) claims.get("authorities");
        loger.info("setUpSpringAuthentication authorities: {}",  authorities);
        var auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
                authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
        SecurityContextHolder.getContext().setAuthentication(auth);

    }

    private boolean checkJWTToken(HttpServletRequest request) {
        var hasHeader = true;
        String authenticationHeader = request.getHeader(JwtConstants.HEADER);
        if (authenticationHeader == null || !authenticationHeader.startsWith(JwtConstants.PREFIX)) {
            hasHeader = false;
        }
        loger.info("doFilterInternal Claims get request hasHeader: {}",  hasHeader);
        return hasHeader;
    }

}