package ee.alab.testwork.security;

import java.util.Date;

public final class JwtConstants {
    private JwtConstants(){}
    static final String HEADER = "Authorization";
    static final String SECRET_KEY = "oeRaYY7Wo24sDqKSX3IM9ASGmdGPmkTd9jo1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI-ZQ19k_o9MI0y3eZN2lp9jow55FfXMiINEdt1XR85VipRLSOkT6kSpzs2x-jbLDiz9iFVzkd81YKxMgPA7VfZeQUm4n-mOmnWMaVX30zGFU4L3oPBctYKkl4dYfqYWqRNfrgPJVi5DGFjywgxx0ASEiJHtV72paI3fDR2XwlSkyhhmY-ICjCRmsJN4fX1pdoL8a18-aQrvyu4j0Os6dVPYIoPvvY0SAZtWYKHfM15g7A3HD4cVREf9cUsprCRK93w";
    static final String PREFIX = "Bearer ";
    static final String AUTHORITIES = "authorities";
    static final String ISSUER = "Mailis Toompuu AvalancheLabs Test";
    static final Date NOW = new Date();
    static final Date EXPIRATION = new Date(System.currentTimeMillis() + 60*60*24*2*1000);
}
