package ee.alab.testwork.services;

import ee.alab.testwork.models.Value;
import ee.alab.testwork.repositories.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ValueService {
    @Autowired
    ValueRepository valueRepository;

    public boolean valueExists(long valueId){
        return valueRepository.existsById(valueId);
    }
    public Value findById(long valueId){
        Optional<Value>  valOp= valueRepository.findById(valueId);
        return valOp.orElse(null);
    }

    public Value saveValue(Value value){
        if(value == null || value.getId() == 0){
            return null;
        }
        return valueRepository.save(value);
    }
}
