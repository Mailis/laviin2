package ee.alab.testwork.services;

import ee.alab.testwork.models.Quote;
import ee.alab.testwork.models.Value;
import ee.alab.testwork.repositories.QuoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuoteService {
    final Logger logger = LoggerFactory.getLogger(QuoteService.class);
    @Autowired
    QuoteRepository quoteRepository;

    public Quote saveQuote(Quote quote){
        Quote existingQuote = quoteRepository.findOneByValue(quote.getValue());
        try {
            if (existingQuote == null) {
                existingQuote = quoteRepository.save(quote);
            }
        }
        catch(Exception e){
            logger.error("Error saving quote: {}", e.getMessage());
        }
        return existingQuote;
    }

    public boolean quoteWithValueExists(Value value){
        return quoteRepository.findOneByValue(value) != null;
    }

    public List<Quote> findAll() {
       return quoteRepository.findAll();
    }
}
