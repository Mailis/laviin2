package ee.alab.testwork.services;

import ee.alab.testwork.models.Role;
import ee.alab.testwork.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public List<Role> findAllRoles(){
        return roleRepository.findAll();
    }

    public Role createRole(Role role) {
        return roleRepository.save(role);
    }

    public Role findRoleById(Long id) {
        Optional<Role> role = roleRepository.findById(id);
        return role.orElse(null);
    }

    public Role findRoleByName(String roleName) {
        return roleRepository.findOneByName(roleName.toUpperCase());
    }

    public Role createRoleIfNotExists(String roleName){
        var role = roleRepository.findOneByName(roleName);
        if (role == null) {
            role = roleRepository.save( new Role(roleName.toUpperCase()) );
        }
        return role;
    }

    public boolean deleteRoleById(Long id) {
        var isSuccessfulDelete = true;
        try {
            roleRepository.deleteById(id);
        }
        catch(Exception e){
            isSuccessfulDelete = false;
        }
        return isSuccessfulDelete;
    }
}
