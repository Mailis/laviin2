package ee.alab.testwork.services;

import ee.alab.testwork.exceptions.ResourceAlreadyExistsException;
import ee.alab.testwork.models.Person;
import ee.alab.testwork.models_request.PersonRequest;
import ee.alab.testwork.models_response.PersonResponse;
import ee.alab.testwork.repositories.PersonRepository;
import ee.alab.testwork.security.JwtOperations;
import ee.alab.testwork.utils.IPersonResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterService extends IPersonResponseBuilder {
    final Logger logger = LoggerFactory.getLogger(RegisterService.class);

    @Autowired
    PersonRepository personRepository;
    @Autowired
    PersonService personService;

    public PersonResponse registerNewUser(PersonRequest personRequest){
        try {
            Person existingPerson = personRepository.existsByEmail(personRequest.getEmail());
            if(existingPerson != null){
                throw new ResourceAlreadyExistsException("Person with this email already exists in DB.");
            }
            else{
                return buildPersonResponse(personService, personRequest);
            }
        }
        catch(ResourceAlreadyExistsException ex){
            logger.error("Registering person EXISTS ALREADY: {}", ex.getMessage());
            return null;
        }
    }

    @Override
    protected void addToken(PersonResponse personResponse, Person newPerson) {
        String token = JwtOperations.generateToken(newPerson);
        logger.info("Registering person TOKEN: {}",  token);
        personResponse.setToken(token);
    }
}
