package ee.alab.testwork.services;

import ee.alab.testwork.utils.LoggerHelper;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Service
public class ExchangeService {
    final Logger logger = LoggerFactory.getLogger(ExchangeService.class);
    final String apiKey = "368457FE-9580-4A93-8694-8B2ACCA94935";


    public Mono<String> sendGetRequest(String url){
        logger.info("send request with URL: {}", url);
        var exchangeStrategies = ExchangeStrategies.builder()
                .codecs(configurer -> configurer
                        .defaultCodecs()
                        .maxInMemorySize(16 * 1024 * 1024))
                .build();

        var httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
                .responseTimeout(Duration.ofMillis(5000))
                .doOnConnected(conn ->
                        conn.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
                                .addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS)));

        WebClient client = WebClient.builder()
                .filters(exchangeFilterFunctions ->
                    exchangeFilterFunctions.add(LoggerHelper.logRequest(logger))
                )
                .exchangeStrategies(exchangeStrategies)
                .filter(ExchangeFilterFunctions
                        .basicAuthentication("username", "token"))
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();
        WebClient.UriSpec<WebClient.RequestBodySpec> uriSpec = client.method(HttpMethod.GET);
        WebClient.RequestBodySpec bodySpec = uriSpec.uri(url);
        WebClient.RequestHeadersSpec<?> headersSpec = bodySpec.bodyValue("data");

        return headersSpec
                .header("X-CoinAPI-Key", this.apiKey)
                .accept(MediaType.APPLICATION_JSON)
                .exchangeToMono(response -> {
                            if (response.statusCode()
                                    .equals(HttpStatus.OK)) {
                                return response.bodyToMono(String.class);
                            } else if (response.statusCode()
                                    .is4xxClientError()) {
                                return Mono.just("Error response");
                            } else {
                                return response.createException()
                                        .flatMap(Mono::error);
                            }
                        }
                );
    }
}
