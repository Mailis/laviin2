package ee.alab.testwork.services;

import ee.alab.testwork.models.Person;
import ee.alab.testwork.models.Role;
import ee.alab.testwork.repositories.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PersonService {
    final Logger logger = LoggerFactory.getLogger(PersonService.class);
    @Autowired
    PersonRepository personRepository;

    @Autowired
    RoleService roleService;

    public Person createPerson(Person newPerson){
        var createdPerson = personRepository.save(newPerson);
        return createdPerson;
    }

    public Person addRole(Person person, String roleName){
        if(person == null || roleName.isEmpty()){
            return null;
        }

        if(personHasRole(person.getRoles(), roleName)){
            return person;
        }

        long personId = person.getId();
        try {
            var uRole = roleService.createRoleIfNotExists(roleName);
            if (uRole != null && personId != 0) {
                long roleId = uRole.getId();
                personRepository.addRoleToPerson(personId, roleId);
                Optional<Person> op = personRepository.findById(personId);
                return op.orElse(null);
            }
        }
        catch(Exception e){
            logger.error("Add Role To person Error: roleName: {}, personId: {} ; errorMessage: {}",
                    roleName, personId, e.getMessage());
        }
        return null;
    }

    public Person addRole(long personId, long roleId){
        if(roleId != 0 && personId != 0){
            personRepository.addRoleToPerson(personId, roleId);
            Optional<Person> op = personRepository.findById( personId);
            return op.orElse(null);
        }
        return null;
    }

    private boolean personHasRole(Set<Role> roles, String roleName){
        var hasThisRole = false;
        for (Role r : roles) {
            if(r.getName().equalsIgnoreCase(roleName)){
                hasThisRole = true;
                break;
            }
        }
        return hasThisRole;
    }

    public List<Person> findAll(){
        return personRepository.findAll();
    }

    public Person findPersonByEmail(String email){
        return  personRepository.findOneByEmail(email);
    }

    public Person existsByEmail(String email){
        return personRepository.existsByEmail(email);
    }

    public Person findOneById(long id) {
        Optional<Person> op = personRepository.findById(id);
        return op.orElse(null);
    }

    public void removeRoleFromPerson(long personId, long roleId){
        personRepository.removeRoleFromPerson(personId, roleId);
    }

    public void deletePerson(Long personId){
        personRepository.deleteById(personId);
    }

}
