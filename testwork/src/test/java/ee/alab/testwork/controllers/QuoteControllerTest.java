package ee.alab.testwork.controllers;

import ee.alab.testwork.models.Quote;
import ee.alab.testwork.models.Value;
import ee.alab.testwork.models_request.QuoteRequest;
import ee.alab.testwork.services.QuoteService;
import ee.alab.testwork.services.ValueService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

class QuoteControllerTest {
    @Mock
    QuoteService quoteService;
    @Mock
    ValueService valueService;
    @InjectMocks
    QuoteController quoteController;

    private static final String quoteValue1 = "I have two hours today to build an app from scratch. @springboot to the rescue!";
    private static final long valId = 11L;
    private static final long quoteId = 1L;
    private static final String quoteType = "success";
    private static final Value v = new Value();
    private static final Value v_expected = new Value();
    private static final Quote q = new Quote();
    private static final Quote q_expected = new Quote();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        v.setId(valId);
        v.setQuote(quoteValue1);
        q.setType(quoteType);
        q.setId(quoteId);
        v_expected.setId(valId);
        v_expected.setQuote(quoteValue1);
        q_expected.setType(quoteType);
        q_expected.setId(quoteId);
        q_expected.setValue(v_expected);
        q.setValue(v_expected);
        when(valueService.saveValue(v)).thenReturn(v_expected);
        when(valueService.findById(valId)).thenReturn(v_expected);
        when(quoteService.saveQuote(q)).thenReturn(q_expected);
        when(quoteService.findAll()).thenReturn(Collections.<Quote>singletonList(q_expected));
    }

    @Test
    void testGetSavedQuotesList() {
        when(quoteService.saveQuote(q)).thenReturn(q_expected);
        when(quoteService.findAll()).thenReturn(Arrays.<Quote>asList(q_expected));

        ResponseEntity<List<Quote>> result = quoteController.index();
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(q.getId(), result.getBody().get(0).getId());
    }

    @Test
    void testGetQuoteReturnsStatusOK() {
        when(quoteService.saveQuote(any())).thenReturn(new Quote());
        when(quoteService.quoteWithValueExists(v)).thenReturn(true);
        when(valueService.findById(valId)).thenReturn(v_expected);

        ResponseEntity<QuoteRequest> result = quoteController.quote(new RestTemplate());
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
