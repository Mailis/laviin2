package ee.alab.testwork.services;

import ee.alab.testwork.models.Quote;
import ee.alab.testwork.models.Value;
import ee.alab.testwork.repositories.QuoteRepository;
import ee.alab.testwork.repositories.ValueRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

class QuoteServiceTest {
    @Mock
    QuoteRepository quoteRepository;
    @Mock
    ValueRepository valueRepository;
    @InjectMocks
    QuoteService quoteService;
    @InjectMocks
    ValueService valueService;

    private static final String quoteValue1 = "I have two hours today to build an app from scratch. @springboot to the rescue!";
    private static final long valId = 11L;
    private static final long quoteId = 1L;
    private static final String quoteType = "success";
    private static final Value v = new Value();
    private static final Value v_expected = new Value();
    private static final Quote q = new Quote();
    private static final Quote q_expected = new Quote();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        v.setId(valId);
        v.setQuote(quoteValue1);
        q.setType(quoteType);
        q.setId(quoteId);
        v_expected.setId(valId);
        v_expected.setQuote(quoteValue1);
        q_expected.setType(quoteType);
        q_expected.setId(quoteId);
        q_expected.setValue(v_expected);
        q.setValue(v_expected);
        when(valueRepository.save(v)).thenReturn(v_expected);
        when(quoteRepository.save(q)).thenReturn(q_expected);
        when(quoteRepository.findAll()).thenReturn(Collections.<Quote>singletonList(q_expected));
    }

    @Test
    void testSaveQuote() {
        Value resVal = valueService.saveValue(v);
        Assertions.assertEquals(v.getId(), resVal.getId());
        Assertions.assertEquals(v.getQuote(), resVal.getQuote());
        Quote qResult = quoteService.saveQuote(q);
        Assertions.assertEquals(q.getId(), qResult.getId());
        Assertions.assertEquals(q.getValue(), qResult.getValue());
        Assertions.assertEquals(q.getType(), qResult.getType());
    }

    @Test
    void testQuoteWithValueExists() {
        when(quoteRepository.findOneByValue(v)).thenReturn(q_expected);

        boolean result = quoteService.quoteWithValueExists(v);
        Assertions.assertTrue(result);
    }

    @Test
    void testFindAll() {
        List<Quote> result = quoteService.findAll();
        Quote resItem = result.get(0);
        Quote expItem = Collections.<Quote>singletonList(q_expected).get(0);
        Assertions.assertEquals(expItem.getId(), resItem.getId());
        Assertions.assertEquals(expItem.getValue(), resItem.getValue());
        Assertions.assertEquals(expItem.getType(), resItem.getType());
    }
}
