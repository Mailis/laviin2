package ee.alab.testwork.services;

import ee.alab.testwork.models.Person;
import ee.alab.testwork.models.Role;
import ee.alab.testwork.models_request.PersonRequest;
import ee.alab.testwork.models_response.PersonResponse;
import ee.alab.testwork.repositories.PersonRepository;
import ee.alab.testwork.repositories.RoleRepository;
import ee.alab.testwork.utils.PBKDF2Hasher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.mockito.Mockito.when;

class PersonServiceTest {
    @Mock
    RoleRepository roleRepository;
    @Mock
    PersonRepository personRepository;
    @Mock
    RoleService roleService;
    @InjectMocks
    PersonService personService;

    final PBKDF2Hasher hasher = new PBKDF2Hasher();


    private static final String firstname = "organizer1";
    private static final String lastName = "organizerLastName";
    private static final String email = "organizer@gmail.com";
    private static final String password = "organizer_Pass";
    private static final Long personId = 1L;
    private static final Person p = new Person();
    private static final Person p_expected = new Person();
    private static final PersonRequest p_request = new PersonRequest();
    private static final PersonResponse p_response = new PersonResponse();

    private static final String roleName = "somerole";
    private static final long roleId = 2L;
    private static final Role r = new Role();
    private static final Role r_expected = new Role();


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        Set<Role> roles = new HashSet<>();
        roles.add(r_expected);
        p.setFirstName(firstname);
        p.setLastName(lastName);
        p.setEmail(email);
        p.setPassword(password);
        p_expected.setFirstName(firstname);
        p_expected.setLastName(lastName);
        p_expected.setEmail(email);
        p_expected.setPassword( hasher.hash(password.toCharArray()) );
        p_expected.setId(personId);
        p_expected.setRoles(roles);

        p_request.setEmail(email);
        p_request.setFirstName(firstname);
        p_request.setLastName(lastName);
        p_request.setPassword(password);

        p_response.setEmail(email);
        p_response.setFirstName(firstname);
        p_response.setLastName(lastName);
        p_response.setRoles(new HashSet<>());

        r.setName(roleName);
        r.setId(roleId);
        r_expected.setName(roleName);
        r_expected.setId(roleId);

        when(personRepository.save(p)).thenReturn(p_expected);
        when(personRepository.findAll()).thenReturn(Collections.<Person>singletonList(p_expected));
        when(personRepository.findOneByEmail(email)).thenReturn(p_expected);
        when(personRepository.findById(personId)).thenReturn(Optional.of(p_expected));

        when(roleRepository.save(r)).thenReturn(r_expected);

        when(roleRepository.save(r)).thenReturn(r_expected);
        when(roleRepository.findOneByName(roleName)).thenReturn(r_expected);
        when(roleRepository.findAll()).thenReturn(Collections.<Role>singletonList(r_expected));
    }

    @Test
    void testCreatePerson() {
        Person result = personService.createPerson(p);
        Assertions.assertEquals(p_expected, result);
    }

    @Test
    void testAddRole() {
        when(roleService.createRoleIfNotExists(roleName)).thenReturn(r_expected);
        p.setId(personId);
        Person result = personService.addRole(p, roleName);
        Assertions.assertEquals(p.getId(), result.getId());
    }

    @Test
    void testAddRole2() {
        Person result = personService.addRole(personId, roleId);
        Assertions.assertEquals(p_expected.getId(), result.getId());
    }

    @Test
    void testFindAll() {
        List<Person> result = personService.findAll();
        Person resItem = result.get(0);
        Person expItem = Collections.<Person>singletonList(p_expected).get(0);
        Assertions.assertEquals(expItem.getId(), resItem.getId());
    }

    @Test
    void testFindPersonByEmail() {
        Person result = personService.findPersonByEmail(email);
        Assertions.assertEquals(p_expected.getId(), result.getId());
    }

    @Test
    void testExistsByEmailIsNotNull() {
        Person saveTestPerson = personService.createPerson(p);
        Assertions.assertEquals(p_expected.getId(), saveTestPerson.getId());
        when(personRepository.existsByEmail(email)).thenReturn(saveTestPerson);
        Person result = personService.existsByEmail(email);
        Assertions.assertEquals(p_expected.getId(), result.getId());
    }

    @Test
    void testExistsByEmailIsNull() {
        when(personRepository.existsByEmail(email)).thenReturn(null);
        Person result = personService.existsByEmail(email);
        Assertions.assertEquals(null, result);
    }

    @Test
    void testFindOneById() {
        Person result = personService.findOneById(personId);
        Assertions.assertEquals(p_expected.getId(), result.getId());
    }

    @Test
    void testRemoveRoleFromPerson() {
        personService.removeRoleFromPerson(0L, 0L);
    }

    @Test
    void testDeletePerson() {
        personService.deletePerson(Long.valueOf(1));
    }
}
