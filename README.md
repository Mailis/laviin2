#  GRADLE TASKS #

gradle build jar package without tests:
```
clean build -x test
```

copy the built jar package to docker folder:
```
cp laviin2\testwork\build\libs\testwork-0.0.1-SNAPSHOT.jar laviin2\testwork\src\main\docker
```
--------

#  DOCKER TASKS #
```
cd src/main/docker
docker-compose down
docker rmi avalanche_test
docker-compose up
```
--------


#  ALL TASKS TOGETHER#
```
docker-compose down
docker rmi avalanche_test
./gradle clean build -x test
cp <absolute path to>laviin2\testwork\build\libs\testwork-0.0.1-SNAPSHOT.jar <absolute path to>laviin2\testwork\src\main\docker
cd src/main/docker
docker-compose up
```
--------

#  ENDPOINTS #

### for all: ###
POST /login

--------
POST /register

--------
GET /roles*

--------
GET /roles/{id}

--------
GET /roles/name/{role}

--------
### for registered users and admin: ###
GET /persosns/*

--------
GET /persosns/{id}

--------
### for admin: ###
POST   /persosns

--------
POST /persosns/{personId}/{roleId}

--------
DELETE /persosns/{personId}/{roleId}

--------

# CURL TASKS #
### REPLACE data in <> , including, with accurate parameters. ###

--------
### login as default user with all privileges, sends back response with an authorization token ###
curl -XPOST -H Content-type: application/json -d '{ "email":"doe_admin3@gmail.com", "password":"DoePass" }' 'http://localhost:8080/login'

--------
### get all persons ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/persons'

--------
### get person by id ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/persons/<id>'

--------
### delete person by id ###
curl -XDELETE -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/persons/<id>'

--------

### post new person ###
curl -XPOST -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" -d '{
"firstName":"Voldemar",
"lastName":"Jõgi",
"email":"voldemar11@gmail.com",
"password":"foo"
}' 'http://localhost:8080/persons'

--------

### register new person, sends back response with an authorization token ###
curl -XPOST -H "Content-type: application/json" -d '{
"firstName":"Bob",
"lastName":"Smith",
"email":"smith@gmail.com",
"password":"foo"
}' 'http://localhost:8080/register'

--------

### add role to person ###
curl -XPOST -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/persons/<personId>/<roleId>'

--------
### remove role from person ###
curl -XDELETE -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/persons/<personId>/<roleId>'

--------
### read all roles ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/roles/'

--------
### get role by id ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/roles/<roleId>'

--------
### delete role by id ###
curl -XDELETE -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/roles/<roleId>'

--------
### get and save data from external service of random quotes ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/quotes/random'

--------
### read all saved quotes ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/quotes/list'

--------
### get  data from another external service ###
### parameters: currencies  <base> and <to> , e.g. http://localhost:8080/exchangerate/ETH/EUR   ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/exchangerate/<base>/<to>'

--------
### get  data from another external service (exchange rate history, provide start year) ###
### parameters: currencies  <base> and <to> and <yearfrom>, e.g. http://localhost:8080/exchangerate/history/ETH/EUR/2018   ###
curl -XGET -H 'Authorization: <Bearer ...>' -H "Content-type: application/json" 'http://localhost:8080/exchangerate/history/<base>/<to>/<yearfrom>'

--------